﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="product_cate.aspx.cs" Inherits="_manage_product_cate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=MainPName%>管理中心</title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Include/Validator.js"></script>
<script type="text/javascript" src="Include/textLimitChk.js"></script>
</head>
<body>
<form id="form1" runat="server">
  <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram_Position">
    <tr>
      <td>您现在的位置：<a href="#"><%=MainPName%>管理中心</a> &gt;&gt; <a href="#">管理<%=SecPName%></a></td>
    </tr>
  </table>
  <table width="40%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram">
    <tr>
      <td align="center"><h2><%=MainPName%>管理中心----管理<%=SecPName%></h2></td>
    </tr>
  </table>
  <table width="96%" border="0" cellpadding="0" cellspacing="0" class="TableSt1">
    <tr>
      <td height="25" align="left" valign="middle"><h3>管理 </h3></td>
      <td width="18%" align="center" valign="middle"></td>
    </tr>
    <tr>
      <td height="25" colspan="2" align="right" valign="middle"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="CateList">
          <tr>
            <td align="left" valign="middle">　<%=SecPName%>列表</td>
          </tr>
          <tr>
            <td height="25" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="TrHover">
                <asp:Literal id="Ft_CaseList" runat="server"></asp:Literal>
              </table></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td height="40" colspan="2" align="right" valign="middle"></td>
    </tr>
  </table>
</form>
</body>
</html>
