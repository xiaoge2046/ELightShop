﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Data;
using YduCLB.Func;

public partial class _manage_product_param : System.Web.UI.Page
{
    public string EventName = "product";
    public string MainPName = "产品属性";
    public string SecPName = "添加产品属性";
    public string UpFileUrl = "co_pros/";
    Conn Tc = new Conn("level0");
    DbCtrl Dc = new DbCtrl();


    protected void Page_Init(object sender, EventArgs e)
    {
        //WebChk.ChkAdmin();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int nId = Convert.ToInt32(Request.QueryString["id"]);

        DataTable Dtab = new DataTable();
        Dtab = Tc.ConnDate("select id,cateid,is_chk from " + TableName.db_product + " where id=" + nId, 0);

        if (Dtab.Rows.Count != 0)
        {
            DataTable DtabII = new DataTable();
            DtabII = Tc.ConnDate("select * from " + TableName.db_proxpitem + " where cateid=" + Dtab.Rows[0]["cateid"].ToString() + " order by nord desc", 0);


            DataColumn Dcol = new DataColumn();
            Dcol.DataType = typeof(string);
            Dcol.ColumnName = "cate_name_";
            Dcol.DefaultValue = "-";
            DtabII.Columns.Add(Dcol);

            for (int i = 0; i < DtabII.Rows.Count; i++)
            {
                DtabII.Rows[i]["pname"] = DtabII.Rows[i]["pname"].ToString();
                DtabII.Rows[i]["cate_name_"] = "";
            }

            Ft_List.DataSource = DtabII.DefaultView;
            Ft_List.DataBind();
        }
        Tc.CloseConn();
    }
}
