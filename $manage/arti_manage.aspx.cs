﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;

public partial class _manage_arti_manage : System.Web.UI.Page
{
    public string EventName = "arti";
    public string MainPName = "资讯";
    public string SecPName = "资讯";
    public int CurPage, PageCount;

    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        Models Md = new Models();
        DataTable Dtab = new DataTable();
        Dtab = Tc.ConnDate("select id,title,cateid,nviews,static,relea_time from " + TableName.db_arti + " order by relea_time desc", 0);

        DataColumn Dcol = new DataColumn();
        Dcol.DataType = typeof(string);
        Dcol.ColumnName = "cate_name_";
        Dcol.DefaultValue = "-";
        Dtab.Columns.Add(Dcol);

        for (int i = 0; i < Dtab.Rows.Count; i++)
        {
            Dtab.Rows[i]["cate_name_"] = Md.getCate_Name((int)(Dtab.Rows[i]["cateid"]), TableName.db_articate);
        }

        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = Dtab.DefaultView;
        objPds.AllowPaging = true;
        objPds.PageSize = 12;

        PageCount = objPds.PageCount;
        CurPage = Convert.ToInt32(Request.QueryString["Page"]);

        if (CurPage < 1)
        {
            CurPage = 1;
        }
        else if (CurPage > objPds.PageCount)
        {
            CurPage = objPds.PageCount;
        }

        objPds.CurrentPageIndex = CurPage - 1;

        Ft_List.DataSource = objPds;
        Ft_List.DataBind();
		Tc.CloseConn();
    }
}
