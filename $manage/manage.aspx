﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="manage.aspx.cs" Inherits="_manage_manage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>网站-后台管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">

function setCookie(name, value) {
	var today = new Date();
	var expires = new Date();
	expires.setTime(today.getTime() + 1000 * 60 * 60 * 24 * 365);
	document.cookie = name + "=" + escape(value) + "; expires=" + expires.toGMTString();
}

var bSw = true;

function Init_Frame_Statu(){
	if(getCookie("Status_Frame") == "Frame_OFF"){
		document.getElementById("left").style.display = "none";
		document.getElementById("Mframe").cols="0,14,100%";
		swIcon=document.getElementById("Mid").contentWindow.document.getElementById("swFrame_Icon");
		swIcon.src="style/images/swFrame_Btn_Open.gif";
		bSw = false;
		//alert(bSw);
	}
}

function getCookie(Name) {
	var search = Name + "=";
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search);
		if (offset != -1) {
		 offset += search.length;
		 end = document.cookie.indexOf(";", offset);
		 if (end == -1) {
		  end = document.cookie.length;
		 }
		 return unescape(document.cookie.substring(offset, end));
		} else {
		 return ("");
		}
	} else {
		return ("");
	}
}

function swFrame(){

 var left=document.getElementById("left");
 var right=document.getElementById("main");
 var midd=document.getElementById("Mid");
 var swIcon=document.getElementById("Mid").contentWindow.document.getElementById("swFrame_Icon");	

 if (bSw){
	  left.style.display = "none";
	  document.getElementById("Mframe").cols="0,14,100%";
	  swIcon.src="style/images/swFrame_Btn_Open.gif";
	  if(getCookie("Status_Frame") != "Frame_OFF"){
	  	 setCookie("Status_Frame","Frame_OFF");
	  }
  }else{
	  left.style.display = "";
	  document.getElementById("Mframe").cols="150,14,100%";
	  swIcon.src="style/images/swFrame_Btn_Close.gif";
	  if(getCookie("Status_Frame") != "Frame_ON"){
	  	 setCookie("Status_Frame","Frame_ON");
	  }
  }
  bSw = !bSw;
}
</script>
</head>

<frameset border="0" frameSpacing="0" rows="25,100%" frameBorder="0" onload="Init_Frame_Statu();">
  <frame name="Top" id="Top" src="Top.aspx" noResize scrolling="no">
  <frameset id="Mframe" name="Mframe" cols="150,11,100%">
    <frame name="Left" id="Left" src="left.aspx">
	<frame name="Mid" id="Mid" src="mid.aspx">
    <frame name="main" id="main" src="main.aspx">
  </frameset>
</frameset>
<noframes></noframes>
</html>
