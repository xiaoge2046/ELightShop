﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Data;
using YduCLB.Func;

public partial class _manage_product_cate : System.Web.UI.Page
{
    public string EventName = "product";
    public string MainPName = "产品选择分类";
    public string SecPName = "选择分类";

    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string sNodetree = nodetree(0, "1");
        Ft_CaseList.Text = sNodetree;
    }

    protected string nodetree(int pr, string sq)
    {
        Conn Tc = new Conn("level0");
        DataTable Dtab = new DataTable();
        DataTable DtabII = new DataTable();
        string sOut = string.Empty;
        int num = 1;
        Dtab = Tc.ConnDate("select * from " + TableName.db_procate + " where nparent=" + pr + " order by nord desc", 0);
        for (int i = 0; i < Dtab.Rows.Count; i++)
        {
            DtabII = Tc.ConnDate("select * from " + TableName.db_procate + " where nparent=" + Dtab.Rows[i]["id"].ToString() + " order by nord desc", 0);
            if (DtabII.Rows.Count > 0)
            {
                sOut = sOut + "<tr><td align=\"left\" valign=\"middle\"><p style=\"padding-left: " + (Convert.ToInt32(Dtab.Rows[i]["cate_levels"]) * 20) + "px;\"><span>" + Dtab.Rows[i]["cate_name"].ToString() + "</span></p></td></tr>\n";
                sOut = sOut + nodetree(Convert.ToInt32(Dtab.Rows[i]["id"]), sq + "-" + num);
            }
            else
            {
                sOut = sOut + "<tr><td align=\"left\" valign=\"middle\"><p style=\"padding-left: " + (Convert.ToInt32(Dtab.Rows[i]["cate_levels"]) * 20) + "px;\"><span>" + Dtab.Rows[i]["cate_name"].ToString() + "　　<a href=\"" + EventName + "_ctrl.aspx?cate=" + Dtab.Rows[i]["id"].ToString() + "\">[选择]</a></span></p></td></tr>\n";
            }
            num = num + 1;
        }
        Tc.CloseConn();
        return sOut;
    }
}
