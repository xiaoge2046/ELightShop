﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="top.aspx.cs" Inherits="_manage_top" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
</head>

<body>
<form id="xform" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="TopFram">
      <tr>
        <td valign="top">
	      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="TopFram_Nav">
            <tr>
              <td width="45" align="center" valign="bottom"><img alt="" src="Style/images/Btn_back.gif" width="30" height="33" /></td>
              <td width="45" align="center" valign="bottom"><img alt="" src="Style/images/Btn_next.gif" width="30" height="33" /></td>
              <td width="62" align="center" valign="bottom"><a href="#"><img alt="" src="Style/images/Btn_reload.gif" width="40" height="36" onClick="parent.main.location.href=parent.main.location.href;" /></a></td>
              <td width="2" align="center" valign="bottom"><img alt="" src="Style/images/LineV1.gif" width="2" height="32" /></td>
              <td width="110" align="center" valign="bottom"><a href="main.aspx" target="main"><img alt="" src="Style/images/Btn_main.gif" width="37" height="36" /></a></td>
              <td width="110" align="center" valign="bottom"><img alt="" src="Style/images/Btn_home.gif" width="92" height="38" /></td>
              <td width="2" align="center" valign="bottom"><img alt="" src="Style/images/LineV1.gif" width="2" height="32" /></td>
              <td width="60" align="center" valign="bottom"><img alt="" src="Style/images/Btn_search.gif" width="43" height="36" /></td>
              <td align="center" valign="bottom">&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</form>
</body>
</html>
