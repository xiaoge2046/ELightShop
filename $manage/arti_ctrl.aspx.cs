﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Data;
using YduCLB.Func;

public partial class _manage_arti_ctrl : System.Web.UI.Page
{
    public string EventName = "arti";
    public string MainPName = "资讯";
    public string SecPName = "添加资讯";
    public string UpFileUrl = "co_arti/";
    Conn Tc = new Conn("level0");
    DbCtrl Dc = new DbCtrl();

    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();

        PhotoUpload.Attributes["src"] = "upload/photoupload.aspx?pic=img&intent=" + UpFileUrl;
        SqlDataReader drReader = Tc.ConnDate("select * from " + TableName.db_articate + " order by nord desc");
        while (drReader.Read())
        {
            ListItem ltItem = new ListItem();
            ltItem.Value = drReader["id"].ToString();
            ltItem.Text = drReader["cate_name"].ToString();
            tid.Items.Add(ltItem);
        }
        editer_cid.Value = "1018";
        drReader.Dispose();

        int nCtrl = Convert.ToInt32(Request.QueryString["act"]);
        int nId = Convert.ToInt32(Request.QueryString["id"]);

        if (nCtrl == 1)
        {
            SecPName = "编辑资讯";
            action.Value = "ctrl_edit";

            SqlDataReader drReaderEd = Tc.ConnDate("select * from " + TableName.db_arti + " where id=" + nId);
            while (drReaderEd.Read())
            {
                actid.Value = drReaderEd["id"].ToString();
                titler.Value = drReaderEd["title"].ToString();
                auth_er.Value = drReaderEd["author"].ToString();
                sourc.Value = drReaderEd["source"].ToString();
                editer.Value = drReaderEd["editor"].ToString();
                editer_cid.Value = drReaderEd["editor_id"].ToString();
                img.Value = drReaderEd["small_Pic"].ToString();
                sim.Value = drReaderEd["brief"].ToString();
                editor_content.Value = drReaderEd["content"].ToString();
                keywords.Value = drReaderEd["keyChar"].ToString();

                int nNum = 0;
                foreach (ListItem item in tid.Items)
                {
                    if (item.Value == drReaderEd["cateid"].ToString())
                    {
                        tid.SelectedIndex = nNum;
                    }
                    nNum++;
                }
                ck_box_1.Checked = drReaderEd["is_top"].ToString() == "1" ? true : false;
                ck_box_2.Checked = drReaderEd["is_rec"].ToString() == "1" ? true : false;
                ck_box_3.Checked = drReaderEd["is_new"].ToString() == "1" ? true : false;
                ck_box_4.Checked = drReaderEd["is_hot"].ToString() == "1" ? true : false;
                ck_box_5.Checked = drReaderEd["is_chk"].ToString() == "1" ? true : false;
            }
			drReaderEd.Dispose();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region
        if (IsPostBack)
        {
            Models Md = new Models();
            Other Ot = new Other();

            string actid_ = actid.Value;
            string titler_ = titler.Value;
            string tid_ = tid.Value;
            string author_ = auth_er.Value;
            string sourc_ = sourc.Value;
            string editer_ = editer.Value;
            string editer_cid_ = editer_cid.Value;
            string img_ = img.Value;
            string brief_ = sim.Value;
            string nr_ = editor_content.Value;
            if (Setting.EnSaveRemoteImg)
            {
                nr_ = Ot.SaveRemoteImg(nr_, UpFileUrl);
            }
            string keys_ = keywords.Value;
            keys_ = Md.DeaKeyWord(keys_);
            
            

            img_ = (img_ == "") ? "noimages.gif" : img_;
            if (img_ != "noimages.gif")
            {
                Ot.LimitImgW(img_, 300, "W");
            }
            string is_top_ = (ck_box_1.Checked == true) ? "1" : "0";
            string is_rec_ = (ck_box_2.Checked == true) ? "1" : "0";
            string is_new_ = (ck_box_3.Checked == true) ? "1" : "0";
            string is_hot_ = (ck_box_4.Checked == true) ? "1" : "0";
            string is_chk_ = (ck_box_5.Checked == true) ? "1" : "0";

            string[] fieldval = new string[]{"title:"+titler_,
                                                    "cateid:"+tid_,
                                                    "brief:"+brief_,
                                                    "content:"+nr_,
                                                    "small_pic:"+img_,
                                                    "keyChar:"+keys_,
                                                    "is_top:"+is_top_,
                                                    "is_rec:"+is_rec_,
                                                    "is_new:"+is_new_,
                                                    "is_hot:"+is_hot_,
                                                    "is_chk:"+is_chk_,
                                                    "source:"+sourc_,
                                                    "author:"+author_,
                                                    "editor:"+editer_,
                                                    "editor_id:"+editer_cid_};
            int nResult = 1;
            if (action.Value == "ctrl_add")
            {
                nResult = Dc.AddRecord(TableName.db_arti, fieldval);
                if (nResult > 0)
                {
                    Other.net_alert("操作出错！");
                }
                else
                {
                    Other.net_alert("操作成功！", EventName + "_manage.aspx");
                }
            }
            if (action.Value == "ctrl_edit")
            {
                nResult = Dc.UpdateRecord(TableName.db_arti, "id=" + actid_, fieldval);
                if (nResult > 0)
                {
                    Other.net_alert("操作出错！");
                }
                else
                {
                    Other.net_alert("操作成功！", "close");
                }
            }


        }
        #endregion
		Tc.CloseConn();
    }
}
