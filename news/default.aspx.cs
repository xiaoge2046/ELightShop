﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.Func;
using YduCLB.Front;
using YduCLB.SQL;


public partial class news_default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        GList xGList = new GList();
        FCtrl xFctrl = new FCtrl();

        ArrayList Result = new ArrayList();
        string[] aRate = xFctrl.Cvrate();
        string sData = string.Empty;
        double nPrice = 0;

		DataTable DtabN = new DataTable();
        DataTable DtabM = new DataTable();

        int PageCount = 0;
        int CurPage = 0;
        int PageSize = 8;
        int nTop = 0;

        string TSQL_Table = TableName.db_arti;
        string TSQL_Field = "id,title,brief,static,relea_time,source,editor";
        string TSQL_Condi = "cateid=5 order by relea_time desc";
		try
        {
			DtabN = Tc.ConnDate("select id from " + TSQL_Table + " where " + TSQL_Condi, 0);
	
			PagedDataSource objPdsN = new PagedDataSource();
            objPdsN.DataSource = DtabN.DefaultView;
            objPdsN.AllowPaging = true;
            objPdsN.PageSize = PageSize;
            DtabN.Dispose();

            PageCount = objPdsN.PageCount;
            CurPage = Convert.ToInt32(Request.QueryString["Page"]);

            if (CurPage < 1)
            {
                CurPage = 1;
            }
            if (CurPage > PageCount)
            {
                CurPage = PageCount;
            }

            nTop = (CurPage - 1) * PageSize;
            DtabM = Tc.ConnDate("select top " + PageSize + " " + TSQL_Field + " from " + TSQL_Table + " where id not in (select top " + nTop.ToString() + " id from " + TSQL_Table + " where " + TSQL_Condi + ") and " + TSQL_Condi, 0);

            PagedDataSource objPdsM = new PagedDataSource();
            objPdsM.DataSource = DtabM.DefaultView;
            DtabM.Dispose();

            Ft_List.DataSource = objPdsM;
            Ft_List.DataBind();
			
            Ft_Page.Text = xFctrl.PageMe(CurPage, PageCount, "");
        }
        catch
        {
            Response.Redirect("/");
        }
        
		sData = string.Empty;
        Result = xGList.get_RecList(TableName.db_arti, 1, 6, "is_rec=1 order by relea_time desc", new string[] { "id", "static", "title" });
        foreach (string[] aList in Result)
        {
            sData = sData + String.Format("<li><a href=\"{1}.html\">{2}</a></li>", aList) + "\n";
        }
        Ft_RecomNews.Text = sData;

        sData = string.Empty;
        Result = xGList.get_RecList(TableName.db_product, 1, 3, "is_top=1 order by relea_time desc", new string[] { "id", "static", "title", "was", "price", "small_pic" });
        foreach (string[] aList in Result)
        {
            nPrice = Convert.ToDouble(aList[4]) / Convert.ToDouble(aRate[0]);
            aList[4] = aRate[1] + Math.Round(nPrice, 2).ToString();

            sData = sData + String.Format("<ul><li class=\"i1\"><a href=\"/store/{1}.html\"><img src=\"{5}\" alt=\"\" width=\"75\" height=\"75\" /></a></li><li class=\"i2\"><a href=\"/store/{1}.html\" title=\"{2}\">{2}</a><span>{4}</span></li><li class=\"mclear\"></li></ul>", aList) + "\n";
        }
        Ft_Tsale.Text = sData;
		
		Ft_PageTitle.Text = "News Center, OfferTablets.com";
        
        Tc.CloseConn();
    }
}
