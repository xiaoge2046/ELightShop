﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="news_default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<form id="xform" runat="server">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="NewsPage">
    <div class="xlocation"><span class="ico"></span><a href="/">Home</a> &gt; News Center</div>
    
    <div class="RightDiv">
        <div class="box_nct">
        	<ul class="Ti_3">
                <li class="i1"><h3>News Center</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="dlist">
                <asp:Repeater id="Ft_List" runat="server">
                <ItemTemplate>
                    <div class="thumb">
                        <h4><a href="/news/<%#Eval("static")%>.html"><%#Eval("title")%></a></h4>
                        <div class="info"><%#Eval("source")%> - <%#Eval("editor")%>, <%#Eval("relea_time")%></div>
                        <p class="brief"><%#Eval("brief")%>… <a href="/news/<%#Eval("static")%>.html">More&gt;&gt;</a></p>
                    </div>
                </ItemTemplate>
                </asp:Repeater>
                <p class="mclear"></p>
				<div class="ipagex" style="margin-top: 28px;">
                    <div class="page-v">
						<asp:Literal id="Ft_Page" runat="server"/>
                    </div>
                </div>
            </div><!--dlist-->
        </div>
    </div><!--RightDiv-->
    <div class="LeftDiv">
        <div class="box_rns">
            <ul class="Ti_3">
                <li class="i1"><h3>Recommend Articles</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="list">
                <ul>
					<asp:Literal id="Ft_RecomNews" runat="server"/>
                </ul>
            </div>
        </div>
        <div class="box_tse">
        	<ul class="Ti_3">
                <li class="i1"><h3>Top Sale Tablets</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="list">
                <asp:Literal id="Ft_Tsale" runat="server"/>
            </div>
        </div><!--box_tse-->
        <div class="box_nht">
        	<ul class="Ti_3">
                <li class="i1"><h3>News Hot Tags</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="list">
                <a href="/?cloud=Android Tablet"><b>Android Tablet</b></a>
                <a href="/?cloud=Ainol Novo7 Elf">Ainol Novo7 Elf</a>
                <a href="/?cloud=Ramos W6HD"><b>Ramos W6HD</b></a>
                <a href="/?cloud=Cube U9GT2">Cube U9GT2</a>
                <a href="/?cloud=Zenithink">Zenithink</a>
                <a href="/?cloud=Onda Vi40">Onda Vi40</a>
                <a href="/?cloud=Superpad">Superpad</a>
                <a href="/?cloud=Newsmy P9">Newsmy P9</a>
                <a href="/?cloud=Teclast Tpad"><b>Teclast Tpad</b></a>
                <a href="/?cloud=Haipad M8">Haipad M8</a>
            </div>
        </div>
    </div><!--LeftDiv-->
    <p class="mclear"></p>
</div>
<!--NewsPage-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</form>
</body>
</html>
