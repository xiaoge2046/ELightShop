﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="list_default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<form id="xform" runat="server">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="ListPage">
    <div class="xlocation"><span class="ico"></span><a href="/">Home</a> &gt; Android Tablet PC &gt; <asp:Literal id="Ft_Loca" runat="server"/></div>
    <div class="RightDiv">
    	<div class="box_pro">
        	<ul class="Ti_3">
                <li class="i1"><h3><asp:Literal id="Ft_CurrTi" runat="server"/></h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="dlist">
                <asp:Repeater id="Ft_List" runat="server">
                    <ItemTemplate>
                        <ul>
                            <li class="i1"><a href="/store/<%#Eval("static")%>.html"><img height="160" width="160" alt="" src="<%#Eval("small_pic")%>"></a></li>
                            <li class="i2"><a title="<%#Eval("title")%>" href="/store/<%#Eval("static")%>.html"><%#Eval("title")%></a></li>
                            <li class="i3"><%#Eval("was")%></li>
                            <li class="i4"><%#Eval("price")%></li>
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>
                <p class="mclear"></p>
                <div class="ipagex wline">
                    <div class="page-v">
                        <asp:Literal id="Ft_Page" runat="server"/>
                    </div>
                </div><!--pagex-->
            </div><!--dlist-->
        </div>
    </div><!--RightDiv-->
    
    <div class="LeftDiv">
    	<div class="box_tse">
        	<ul class="Ti_2">
                <li class="i1"><h3>Top Sale Tablets</h3></li>
                <li class="i2"></li>
                <li class="mclear"></li>
            </ul>
            <div class="list">
                <asp:Literal id="Ft_Tsale" runat="server"/>
            </div>
        </div><!--box_tse-->
    </div><!--LeftDiv-->
    <p class="mclear"></p>
</div><!--ListPage-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</form>
</body>
</html>