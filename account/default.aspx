﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="account_default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="UserCen">
	<div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">Account</a></div>
    <div class="RightDiv">
        <div class="acc_box">
          <h2 class="acc_ti01">Yokit, Welcome to My Account</h2>
          <p class="notice">Email Address Confirmation:<i class="ico"></i></p>
          <p class="notice mt10">Enjoy more than hundred thousand high-quality merchandise and famous brand name products, all at wholesale prices and sign up for e-mail newsletters.</p>
          <div class="vlist">
              <dl class="v1">
                <dt><a href="#">My Order List</a></dt>
                <dd>Track your current order status and leave a message.</dd>
              </dl>
              <dl class="v2">
                <dt><a href="#">My Coin</a></dt>
                <dd>Check the Coin you rewarded, used and the balance.</dd>
              </dl>
              <dl class="v3">
                <dt><a href="#">Account Setting</a></dt>
                <dd>Update your name, password and profile.</dd>
              </dl>
              <dl class="v4">
                <dt><a href="#">My Reviews</a></dt>
                <dd>Check the reviews you have left and the items you reviewed.</dd>
              </dl>
              <dl class="v5">
                <dt><a href="#">Manage Address Book</a></dt>
                <dd>Edit or delete your billing address and shipping address</dd>
              </dl>
              <dl class="v6">
                <dt><a href="#">Wish List</a></dt>
                <dd>Purchase or cancel the items you have added it to your Wish List.</dd>
              </dl>
              <p class="mclear"></p>
          </div>
        </div>
    </div><!--RightDiv-->
    
	<Stencil:AccLeftTemp id="AccLeftTemp_" runat="server" />
    
    <p class="mclear"></p>
    
</div><!--UserCen-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</body>
</html>