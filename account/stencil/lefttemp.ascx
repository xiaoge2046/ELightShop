﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="lefttemp.ascx.cs" Inherits="account_stencil_lefttemp" %>
<div class="LeftDiv">

    <div class="box_menu">
        <ul class="Ti_2">
            <li class="i1"><h3>Client Order</h3></li>
            <li class="i2"></li>
            <li class="mclear"></li>
        </ul>
        <div class="list">
        	<ul>
				<li><a href="/account/my-order-list.html">My Order List</a></li>
            </ul>
        </div>
    </div><!--box_menu-->
    
    <div class="box_menu mt10">
        <ul class="Ti_2">
            <li class="i1"><h3>Customer Service</h3></li>
            <li class="i2"></li>
            <li class="mclear"></li>
        </ul>
        <div class="list">
			<ul>
				<%--<li><a href="#">My Refund Record(<em>0</em>)</a></li>
                <li><a href="#">My Message(<em>0</em>)</a></li>--%>
                <%--<li><a href="#">RMA Request(<em>0</em>)</a></li>--%>
                <%--<li><a href="#">My Reviews</a></li>
                <li><a href="#">My Questions</a></li>--%>
                <li><a href="/account/my-wish-lists.html">My Wish Lists</a></li>
            </ul>
        </div>
    </div><!--box_menu-->
    
    <div class="box_menu mt10">
        <ul class="Ti_2">
            <li class="i1"><h3>Information Setting</h3></li>
            <li class="i2"></li>
            <li class="mclear"></li>
        </ul>
        <div class="list">
			<ul>
				<li><a href="/account/my-profile.html">My Profile</a></li>
                <%--<li><a href="#">My Points & Coupons</a></li>--%>
                <%--<li><a href="#">How to get Rewards</a></li>--%>
                <li><a href="/account/confirm-email-address.html">Confirm Email Address</a></li>
                <%--<li><a href="/account/email-subscriptions.html">Email Subscriptions</a></li>--%>
            </ul>
        </div>
    </div><!--box_menu-->
    
</div><!--LeftDiv-->