﻿using System;
using System.Data;
using System.Data.SqlClient;
using YduCLB.SQL;

public class FData
{
    public FData()
    {
	    //
	    // TODO: 在此处添加构造函数逻辑
	    //
    }
}

public class FCtrl
{
    public string[] aFn = new string[8];
    public Conn Tc = new Conn();

    public FCtrl()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
        aFn[0] = "Test";
    }

    public void Invoke()
    {

    }
    public string Test(string sTem_)
    {
        sTem_ = sTem_ + "IsTest";
        return sTem_;
    }

    public string[] Cvrate()
    {
        string[] aResult = new string[2];
        aResult[0] = string.Empty;
        aResult[1] = string.Empty;
        try
        {
            SqlDataReader objDataReader101 = Tc.ConnDate("select * from " + TableName.db_rate + " where ident='" + System.Web.HttpContext.Current.Request.Cookies["Currency"].Value + "'");
			//SqlDataReader objDataReader101 = Tc.ConnDate("select * from " + TableName.db_rate + " where ident='" + "USD" + "'");
            if (objDataReader101.Read())
            {
                aResult[0] = objDataReader101["erate"].ToString();
                aResult[1] = objDataReader101["notat"].ToString();
                objDataReader101.Close();
                objDataReader101.Dispose();
            }
            else
            {
                objDataReader101.Close();
                objDataReader101.Dispose();
                objDataReader101["erate"].ToString();
            }  
        }
        catch
        {
			System.Web.HttpContext.Current.Response.Cookies["Currency"].Value = "USD";
            SqlDataReader objDataReader102 = Tc.ConnDate("select * from " + TableName.db_rate + " where ident='USD'");
            if (objDataReader102.Read())
            {
                aResult[0] = objDataReader102["erate"].ToString();
                aResult[1] = objDataReader102["notat"].ToString();
            }
            objDataReader102.Close();
            objDataReader102.Dispose();
        }
        return aResult;
    }

    public string Relart(string sPid, string sCondi, string sTable)
    {
        string sTitleN = string.Empty, sTitleM = string.Empty;
        sCondi = sCondi == "" ? "0=0" : sCondi;

        SqlDataReader objDataReader103 = Tc.ConnDate("select top 1 id,static,title from " + sTable + " where id>" + sPid + " and " + sCondi + " order by id");
        try
        {
            objDataReader103.Read();
            sTitleN = "<a href=\"/news/" + objDataReader103["static"].ToString() + ".html\">" + objDataReader103["title"].ToString() + "</a>";
        }
        catch
        {
            sTitleN = "There is not.";
        }
        objDataReader103.Close();
        objDataReader103.Dispose();


        SqlDataReader objDataReader104 = Tc.ConnDate("select top 1 id,static,title from " + sTable + " where id<" + sPid + " and " + sCondi + " order by id desc");
        try
        {
            objDataReader104.Read();
            sTitleM = "<a href=\"/news/" + objDataReader104["static"].ToString() + ".html\">" + objDataReader104["title"].ToString() + "</a>";
        }
        catch
        {
            sTitleM = "There is not.";
        }
        objDataReader104.Close();
        objDataReader104.Dispose();
        
        return "<li>Prev: " + sTitleN + "</li>" + "<li>Next: " + sTitleM + "</li>";
    }

    public string[] ArrCountry()
    {
        string sCountry = "Andorra|United Arab Emirates|Afghanistan|Antigua and Barbuda|Anguilla|Albania|Armenia|Netherlands Antilles|Angola|Antarctica|Argentina|American Samoa|Austria|Australia|Aruba|Azerbaijan|Bosnia and Herzegowina|Barbados|Bangladesh|Belgium|Burkina Faso|Bulgaria|Bahrain|Burundi|Benin|Bermuda|Brunei Darussalam|Bolivia|Brazil|Bahamas|Bhutan|Bouvet Island|Botswana|Belarus|Belize|Canada|Cocos (Keeling) Islands|Central African Republic|Congo|Switzerland|Cote D'Ivoire|Cook Islands|Chile|Cameroon|China|Colombia|Costa Rica|Cuba|Cape Verde|Christmas Island|Cyprus|Czech Republic|Germany|Djibouti|Denmark|Dominica|Dominican Republic|Algeria|Ecuador|Estonia|Egypt|Western Sahara|Eritrea|Spain|Ethiopia|Finland|Fiji|Falkland Islands (Malvinas)|Micronesia|Faroe Islands|France|Gabon|United Kingdom|Grenada|Georgia|French Guiana|Ghana|Gibraltar|Greenland|Gambia|Guinea|Guadeloupe|Equatorial Guinea|Greece|South Georgia & South Sandwich Islands|Guatemala|Guam|Guinea-bissau|Guyana|Hong Kong|Heard and Mc Donald Islands|Honduras|Croatia (local Name: Hrvatska)|Haiti|Hungary|Indonesia|Ireland|Israel|India|British Indian Ocean Territory|Iraq|Iran (Islamic Republic of)|Iceland|Italy|Jamaica|Jordan|Japan|Kenya|Kyrgyzstan|Cambodia|Kiribati|Comoros|Saint Kitts and Nevis|Korea, Democratic People's Republic of|Korea|Kuwait|Cayman Islands|Kazakhstan|Lao People's Democratic Republic|Lebanon|Saint Lucia|Liechtenstein|Sri Lanka|Liberia|Lesotho|Lithuania|Luxembourg|Latvia|Libyan Arab Jamahiriya|Morocco|Monaco|Moldova|Madagascar|Marshall Islands|Macedonia|Mali|Myanmar|Mongolia|Macau|Northern Mariana Islands|Martinique|Mauritania|Montserrat|Malta|Mauritius|Maldives|Malawi|Mexico|Malaysia|Mozambique|Namibia|New Caledonia|Niger|Norfolk Island|Nigeria|Nicaragua|Netherlands|Norway|Nepal|Nauru|Niue|New Zealand|Oman|Panama|Peru|French Polynesia|Papua New Guinea|Philippines|Pakistan|Poland|St. Pierre and Miquelon|Pitcairn|Puerto Rico|Portugal|Palau|Paraguay|Qatar|Reunion|Romania|Serbia|Russia|Rwanda|Saudi Arabia|Solomon Islands|Seychelles|Sudan|Sweden|Singapore|St. Helena|Slovenia|Svalbard and Jan Mayen Islands|Slovakia (Slovak Republic)|Sierra Leone|San Marino|Senegal|Somalia|Suriname|Sao Tome and Principe|El Salvador|Syrian Arab Republic|Swaziland|Turks and Caicos Islands|Chad|French Southern Territories|Togo|Thailand|Tajikistan|Tokelau|Turkmenistan|Tunisia|Tonga|Turkey|Trinidad and Tobago|Tuvalu|Taiwan|Tanzania|Ukraine|Uganda|United States minor outlying islands|United States|Uruguay|Uzbekistan|Vatican City State (Holy See)|Saint Vincent and the Grenadines|Venezuela|Virgin Islands (British)|Virgin Islands (U.S.)|Viet Nam|Vanuatu|Wallis and Futuna Islands|Samoa|Yemen|Mayotte|South Africa|Zambia|Zimbabwe";
        sCountry = sCountry + "|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BM|BN|BO|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CF|CG|CH|CI|CK|CL|CM|CN|CO|CR|CU|CV|CX|CY|CZ|DE|DJ|DK|DM|DO|DZ|EC|EE|EG|EH|ER|ES|ET|FI|FJ|FK|FM|FO|FR|GA|GB|GD|GE|GF|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|ID|IE|IL|IN|IO|IQ|IR|IS|IT|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|ST|SV|SY|SZ|TC|TD|TF|TG|TH|TJ|TK|TM|TN|TO|TR|TT|TV|TW|TZ|UA|UG|UM|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|YE|YT|ZA|ZM|ZW";
        string[] arr_country = sCountry.Split('|');
        return arr_country;
    }

    public string PageMe(int nCurPage, int nPageCount, string Param)
    {
        string sHtml = string.Empty;
        string sPrevPage = string.Empty;
        string sNextPage = string.Empty;
        string sOrPage = string.Empty;
        string sCuPage = string.Empty;
        string sMoPage = string.Empty;
        string sLeftPage = string.Empty;
        string sRightPage = string.Empty;
        string sFirPage = string.Empty;
        string sEndPage = string.Empty;

        int SepaPage = 7;

        if (Param!="")
        {
            Param = Param + "&";
        }

        sOrPage = "<a class=\"orpage\" href=\"?" + Param + "page={$1}\">{$1}</a>";
        sCuPage = "<span class=\"cupage\">{$1}</span>";
        sMoPage = "<a class=\"orpage\" href=\"?" + Param + "page={$1}\">...</a>";
        sLeftPage = sMoPage.Replace("{$1}", (nCurPage - 3).ToString());
        sRightPage = sMoPage.Replace("{$1}", (nCurPage + 4).ToString());
        sFirPage = sOrPage.Replace("{$1}", "1");
        sEndPage = sOrPage.Replace("{$1}", (nPageCount - 1).ToString()) + sOrPage.Replace("{$1}", nPageCount.ToString());

        if (nCurPage == 1)
        {
            sPrevPage = "<span class=\"nopage\">« Previous</span>";
        }
        else
        {
            sPrevPage = "<a class=\"nrpage\" href=\"?" + Param + "page=" + (nCurPage - 1).ToString() + "\">« Previous</a>";
        }

        if (nCurPage == nPageCount)
        {
            sNextPage = "<span class=\"nopage\">Next »</span>";
        }
        else
        {
            sNextPage = "<a class=\"nrpage\" href=\"?" + Param + "page=" + (nCurPage + 1).ToString() + "\">Next »</a>";
        }

        if (nPageCount <= SepaPage + 2)
        {
            for (int i = 1; i <= nPageCount; i++)
            {
                if (nCurPage == i)
                {
                    sHtml = sHtml + sCuPage.Replace("{$1}", i.ToString());
                }
                else
                {
                    sHtml = sHtml + sOrPage.Replace("{$1}", i.ToString());
                }
            }
            sLeftPage = "";
            sRightPage = "";
            sFirPage = "";
            sEndPage = "";
        }

        if (nPageCount > SepaPage + 2)
        {
            if (nCurPage < SepaPage - 1)
            {
                for (int i = 1; i <= SepaPage; i++)
                {
                    if (nCurPage == i)
                    {
                        sHtml = sHtml + sCuPage.Replace("{$1}", i.ToString());
                    }
                    else
                    {
                        sHtml = sHtml + sOrPage.Replace("{$1}", i.ToString());
                    }
                }
                sLeftPage = "";
                sFirPage = "";
                sRightPage = sMoPage.Replace("{$1}", (SepaPage + 1).ToString());
            }

            if (nCurPage >= SepaPage - 1 && nCurPage <= nPageCount - SepaPage + 2)
            {
                for (int i = nCurPage - 2; i <= nCurPage + 2; i++)
                {
                    if (nCurPage == i)
                    {
                        sHtml = sHtml + sCuPage.Replace("{$1}", i.ToString());
                    }
                    else
                    {
                        sHtml = sHtml + sOrPage.Replace("{$1}", i.ToString());
                    }
                }
            }

            if (nCurPage > nPageCount - SepaPage + 2)
            {
                for (int i = nPageCount - SepaPage + 1; i <= nPageCount; i++)
                {
                    if (nCurPage == i)
                    {
                        sHtml = sHtml + sCuPage.Replace("{$1}", i.ToString());
                    }
                    else
                    {
                        sHtml = sHtml + sOrPage.Replace("{$1}", i.ToString());
                    }
                }
                sRightPage = "";
                sEndPage = "";
                sLeftPage = sMoPage.Replace("{$1}", (nPageCount - SepaPage).ToString());
            }
        }

        sHtml = sPrevPage + sFirPage + sLeftPage + sHtml + sRightPage + sEndPage + sNextPage;

        return sHtml;
    }
}