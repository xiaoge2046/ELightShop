﻿var $3={};
$3=function(ni){
	//if(typeof(ni)=="object"){if(ni==window){return $3.element;}else{return $3.element;}}
	if(typeof(ni)!="string"){
		var evt,evt_src;
		evt = window.event || arguments.callee.caller.arguments[0];
		evt_src = evt ? (evt.srcElement || evt.target) : null;
		if(evt_src && $3.uneach){
			return evt_src;
		}else{
			return $3.element;
		}
	}
	
	var xnode,elem;
	var arr_ni = ni.split(" ");
	
	if(arr_ni.length == 1){
		elem=$3.sele(document,arr_ni[0],0,0);
	}else{
		xnode=document;
		for(var i=0;i<arr_ni.length;i++){
			if(i!=arr_ni.length-1){
				xnode=$3.sele(xnode,arr_ni[i],1,0);
			}else{
				xnode=$3.sele(xnode,arr_ni[i],1,1);
			}
		}
		elem=xnode;
	}
	if(typeof(elem)=="object"){
		xnode=null;
		return $3.init_enti(elem);
	}else{
		return null;
	}
};

$3.sele=function(xnode,ni,way,last){
	if(xnode==null || xnode==undefined){return null;}
	var arr_m,arr_n,arr_elem,str,elem=[],bool=false;
	try{
		if(ni.indexOf("#")!=-1){
			return xnode.getElementById(ni.substr(1));
		}else if(ni.indexOf(".")!=-1){
			arr_m=ni.split(".");
			if(arr_m[1].indexOf(":")!=-1){
				arr_n=arr_m[1].split(":");
				str=arr_n[0];
				bool=true;
			}else{
				str=arr_m[1];
			}
			str = str.indexOf("|")==-1 ? str:str.replace("|"," ");
			arr_elem=xnode.getElementsByTagName(arr_m[0]);
			for(var i=0;i<arr_elem.length;i++){
				if(arr_elem[i].className==str){
					elem[elem.length]=arr_elem[i];
					if(way==1 && last==0){break;}
				}
			}
			if(bool==true){
				return elem[arr_n[1]];
			}else{
				if(xnode==document && way==0){
					return elem;
				}else{
					return last==0 ? elem[0]:elem;
				}
			}
			
		}else if(ni.indexOf(":")!=-1){
			arr_m=ni.split(":");
			arr_elem=xnode.getElementsByTagName(arr_m[0]);
			return arr_elem[arr_m[1]];
		}else{
			if(xnode==document && way==0){
				return xnode.getElementsByTagName(ni);
			}else{
				elem=xnode.getElementsByTagName(ni);
				return last==0 ? elem[0]:elem;
			}
		}
		arr_m=arr_n=arr_elem=str=elem=bool=null;
	}catch(err){
		return null;
	}
};

$3.init_enti=function(elem){
	if(elem==null || elem==undefined){return null;}
	var extstr;
	
	$3.isie = document.all ? true:false;
	
	if(elem.length==undefined){
		extstr="parent|find|click";
	}else{
		extstr="each";
	}
	extstr=extstr.split("|");
	for(var i = 0; i < extstr.length; i++){
		eval("elem." + extstr[i] + "=" + eval("$3._"+extstr[i]));
	}
	extstr=null;
	return elem;
};

$3.bind_$3elem=function(){
	var extstr;
	if($3.element){
	    if($3.element.length==undefined){
		    extstr="parent|find|click";
	    }else{
		    extstr="each";
	    }
	    $3.extend(extstr,"$3.element");
	}
	extstr=null;
};

$3.uneach=true;

$3.doc=function(){ return document;};
$3.box=function(){ return document.body;};
$3.elem=function(){return document.documentElement;};
$3.onload=function(fn){var oldonload=window.onload;if(typeof(window.onload)!="function"){window.onload=fn;}else{window.onload=function(){oldonload();fn()}}};

/*start extend*/
$3.extend=function(extstr,elemstr){
	extstr=extstr.split("|");
	for(var i = 0; i < extstr.length; i++){
		eval(elemstr + "." + extstr[i] + "=" + eval("$3._"+extstr[i]));
	}
	extstr=elemstr=null;
};

$3._parent=function(ni){
	var parent_tem,elem,bool;
	if(this.length==undefined){
		if(ni==null || ni==undefined || ni==""){
			return this.parentNode;
		}else{
			if(typeof(ni)!="string"){return null;}
			parent_tem=this.parentNode;
			while(true)
			{
				elem=$3.sele(parent_tem.parentNode,ni,1,1);
				if(elem!=undefined){
					if(elem.length==undefined){
						if(parent_tem==elem){
							break;
						}
						parent_tem=parent_tem.parentNode;
					}else{
						for(var i = 0; i < elem.length; i++){
							if(parent_tem==elem[i]){
								bool=true;
								break;
							}
						}
						if(bool){
							break;
						}
						parent_tem=parent_tem.parentNode;
					}
				}else{
					parent_tem=parent_tem.parentNode;
				}
			}
			
			$3.element = parent_tem;
			$3.bind_$3elem();
			
			parent_tem=elem=null;
			return $3.element;
		}
	}else{
		return null;
	}

};

$3._find=function(ni){
	if(ni==null || ni==undefined || ni==""){return null};
	if(this.length==undefined){
		$3.element = $3.sele(this,ni,1,1);
		$3.bind_$3elem();
		return $3.element;
	}else{
		return null;
	}
};

$3._each=function(fn){
	if(this.length!=undefined){
		$3.uneach=false;
		for(var i = 0; i < this.length; i++){
			$3.element=this[i];
			$3.bind_$3elem();
			fn();
		}
		$3.uneach=true;
	}else{
		return null;
	}
};

$3._click=function(fn){
	if(this.length==undefined){
		if($3.isie){
			this.attachEvent("onclick",fn);
		}else{
			this.addEventListener("click",fn);
		}
	}else{
		return null;
	}
};

$3.evtsrc=function(){
	var evt,evt_src,func;
	func=arguments.callee;
	while(true)
	{
		 evt=window.event || func.caller.arguments[0];
		 evt_src=evt.srcElement || evt.target;
		 if(evt_src){
		 	break;
		 }else{
		 	func=func.caller;
		 }
		 
	}
	$3.element = evt_src;
	$3.bind_$3elem();
	evt=func=null;
	return evt_src;
};

$3.request = function(argname) {
    var url = document.location.href;
    var arrStr = url.substring(url.indexOf("?") + 1).split("&");
    for (var i = 0; i < arrStr.length; i++) {
        var loc = arrStr[i].indexOf(argname + "=");
        if (loc != -1) {
            return arrStr[i].replace(argname + "=", "").replace("?", "");
            break;
        }
    }
    return "";
};

$3.setcookie = function(name, value) {
    var today = new Date();
    var expires = new Date();
    expires.setTime(today.getTime() + 1000 * 60 * 60 * 24 * 365);
    document.cookie = name + "=" + escape(value) + "; expires=" + expires.toGMTString();
};

$3.getcookie = function(name) {
    var search = name + "=";
    if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = document.cookie.indexOf(";", offset);
            if (end == -1) {
                end = document.cookie.length;
            }
            return unescape(document.cookie.substring(offset, end));
        } else {
            return ("");
        }
    } else {
        return ("");
    }
};

$3.xmlhttp = function() {
    var xmlhttp;
    try {
        if (window.ActiveXObject) {
            var msxmls = ["MSXML3", "MSXML2", "Microsoft"];
            for (var i = 0; i < msxmls.length; i++){
                try {
                    return new ActiveXObject(msxmls[i] + ".XMLHTTP")
                } catch (e) {
                    
                }
            }
        }else{
            return new XMLHttpRequest();
        }
    } catch(e) {
        throw new Error("No   XML   component   installed!");
    }
};

$3.ajax = function(url, data, type, refun) {

    var x = $3.xmlhttp();
    x.open("POST", url, false);
    x.setRequestHeader("cache-control", "no-cache");
    x.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    x.send(data);
    if (x.readyState == 4) {
        if (x.status == 200) {
            switch (type) {
            case "xml":
                {
                    if (!$3.isie) {
                        var parser = new DOMParser();
                        refun(parser.parseFromString(x.responseText, "text/xml"));
                    } else {
                        refun(x.responseXml.documentElement);
                    }
                    break;
                }
            case "html":
                {
                    refun(x.responseText);
                    break;
                }
            case "josn":
                {
                    refun(eval("(" + x.responseText + ")"));
                    break;
                }
            }
        }
    }
};
/*end extend*/