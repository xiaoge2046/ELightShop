﻿validator = {
	require : /.+/,
	email : /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
	phone : /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/,
	mobile : /^((\(\d{2,3}\))|(\d{3}\-))?13\d{9}$/,
	url : /^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/,
	idcard : "this.IsIdCard(value)",
	currency : /^\d+(\.\d+)?$/,
	number : /^\d+$/,
	zip : /^[1-9]\d{5}$/,
	qq : /^[1-9]\d{4,8}$/,
	integer : /^[-\+]?\d+$/,
	double : /^[-\+]?\d+(\.\d+)?$/,
	english : /^[A-Za-z]+$/,
	chinese :  /^[\u0391-\uFFE5]+$/,
	username : /^[a-z]\w{3,}$/i,
	unsafe : /^(([A-Z]*|[a-z]*|\d*|[-_\~!@#\$%\^&\*\.\(\)\[\]\{\}<>\?\\\/\'\"]*)|.{0,5})$|\s/,
	issafe : function(str){return !this.unsafe.test(str);},
	safestring : "this.issafe(value)",
	filter : "this.DoFilter(value, getAttribute('accept'))",
	limit : "this.Limit(value.length,getAttribute('min'),  getAttribute('max'))",
	limitb : "this.Limit(this.LenB(value), getAttribute('min'), getAttribute('max'))",
	date : "this.IsDate(value, getAttribute('min'), getAttribute('format'))",
	repeat : "value == document.getElementsByName(getAttribute('to'))[0].value",
	range : "getAttribute('min') < (value|0) && (value|0) < getAttribute('max')",
	compare : "this.Compare(value,getAttribute('operator'),getAttribute('to'))",
	custom : "this.Exec(value, getAttribute('regexp'))",
	group : "this.MustChecked(getAttribute('name'), getAttribute('min'), getAttribute('max'))",
	ErrorItem : [document.forms[0]],
	ErrorMessage : ["以下原因导致提交失败：\t\t\t\t"],
	validate : function(theForm, mode){
		var obj = theForm || event.srcElement;
		var count = obj.elements.length;
		this.ErrorMessage.length = 1;
		this.ErrorItem.length = 1;
		this.ErrorItem[0] = obj;
		for(var i=0;i<count;i++){
			with(obj.elements[i]){
				var _dataType = getAttribute("dataType");
				if(typeof(_dataType) == "object" || typeof(this[_dataType]) == "undefined")  continue;
				this.ClearState(obj.elements[i], mode);
				if(getAttribute("require") == "false" && value == "") continue;
				switch(_dataType){
					case "idcard" :
					case "date" :
					case "repeat" :
					case "range" :
					case "compare" :
					case "custom" :
					case "group" : 
					case "limit" :
					case "limitb" :
					case "safestring" :
					case "filter" :
						if(!eval(this[_dataType]))	{
							this.AddError(i, getAttribute("msg"));
						}
						break;
					default :
						if(!this[_dataType].test(value)){
							this.AddError(i, getAttribute("msg"));
						}
						break;
				}
			}
		}
		if(this.ErrorMessage.length > 1){
			mode = mode || 1;
			var errCount = this.ErrorItem.length;
			switch(mode){
			case 2 :
				for(var i=1;i<errCount;i++)
					this.ErrorItem[i].style.color = "red";
			case 1 :
				alert(this.ErrorMessage.join("\n"));
				this.ErrorItem[1].focus();
				break;
			case 3 :
				for(var i=1;i<errCount;i++){
				try{
					var span = document.createElement("SPAN");
					span.id = "__ErrorMessagePanel";
					span.style.color = "red";
					this.ErrorItem[i].parentNode.appendChild(span);
					span.innerHTML = this.ErrorMessage[i].replace(/\d+:/,"*");
					}
					catch(e){alert(e.description);}
				}
				this.ErrorItem[1].focus();
				break;
			case 4 :
			    for(var i=1;i<errCount;i++){
				    try{
					    var span = document.createElement("SPAN");
					    span.id = "__ErrorMessagePanel";
					    span.style.color = "red";
					    var label;
			            var parent_tem = this.ErrorItem[i].parentNode;
			            var inum=0;
			            while(inum<20)
			            {
			                if(parent_tem.getElementsByTagName("label")!=undefined && parent_tem.getElementsByTagName("label").length>0){
			                    label=parent_tem.getElementsByTagName("label")[0];
			                    break;
			                }
			                parent_tem = parent_tem.parentNode;
			                inum++;
			            }
					    label.appendChild(span);
					    span.innerHTML = this.ErrorMessage[i].replace(/\d+:/,"*");
				    }
					catch(e){alert(e.description);}
				}
				this.ErrorItem[1].focus();
				break;
			default :
				alert(this.ErrorMessage.join("\n"));
				break;
			}
			return false;
		}
		return true;
	},
	Limit : function(len,min, max){
		min = min || 0;
		max = max || Number.MAX_VALUE;
		return min <= len && len <= max;
	},
	LenB : function(str){
		return str.replace(/[^\x00-\xff]/g,"**").length;
	},
	ClearState : function(elem, mode){
		with(elem){
		    if(mode==4){
		        var label;
                var parent_tem = parentNode;
	            var inum=0;
	            while(inum<20)
	            {
	                if(parent_tem.getElementsByTagName("label")!=undefined && parent_tem.getElementsByTagName("label").length>0){
	                    label=parent_tem.getElementsByTagName("label")[0];
	                    break;
	                }
	                parent_tem = parent_tem.parentNode;
	                inum++;
	            }
		        if(label){label.innerHTML="";}
		    }else{
			    if(style.color == "red")
				    style.color = "";
			    var lastNode = parentNode.childNodes[parentNode.childNodes.length-1];
			    if(lastNode.id == "__ErrorMessagePanel")
				    parentNode.removeChild(lastNode);
			}
		}
	},
	AddError : function(index, str){
		this.ErrorItem[this.ErrorItem.length] = this.ErrorItem[0].elements[index];
		this.ErrorMessage[this.ErrorMessage.length] = this.ErrorMessage.length + ":" + str;
	},
	Exec : function(op, reg){
		return new RegExp(reg,"g").test(op);
	},
	Compare : function(op1,operator,op2){
		switch (operator) {
			case "notequal":
				return (op1 != op2);
			case "greaterthan":
				return (op1 > op2);
			case "greaterthanequal":
				return (op1 >= op2);
			case "lessthan":
				return (op1 < op2);
			case "lessthanequal":
				return (op1 <= op2);
			default:
				return (op1 == op2);            
		}
	},
	MustChecked : function(name, min, max){
		var groups = document.getElementsByName(name);
		var hasChecked = 0;
		min = min || 1;
		max = max || groups.length;
		for(var i=groups.length-1;i>=0;i--)
			if(groups[i].checked) hasChecked++;
		return min <= hasChecked && hasChecked <= max;
	},
	DoFilter : function(input, filter){
        return new RegExp("^.+\.(?=EXT)(EXT)$".replace(/EXT/g, filter.split(/\s*,\s*/).join("|")), "gi").test(input);
	},
	IsIdCard : function(inumber){
		var date, Ai;
		var verify = "10x98765432";
		var Wi = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
		var area = ['','','','','','','','','','','','北京','天津','河北','山西','内蒙古','','','','','','辽宁','吉林','黑龙江','','','','','','','','上海','江苏','浙江','安微','福建','','山东','','','','河南','湖北','湖南','广东','广西','海南','','','','重庆','四川','贵州','云南','西藏','','','','','','','陕西','甘肃','青海','宁夏','新疆','','','','','','台湾','','','','','','','','','','香港','澳门','','','','','','','','','国外'];
		var re = inumber.match(/^(\d{2})\d{4}(((\d{2})(\d{2})(\d{2})(\d{3}))|((\d{4})(\d{2})(\d{2})(\d{3}[x\d])))$/i);
		if(re == null) return false;
		if(re[1] >= area.length || area[re[1]] == "") return false;
		if(re[2].length == 12){
			Ai = inumber.substr(0, 17);
			date = [re[9], re[10], re[11]].join("-");
		}
		else{
			Ai = inumber.substr(0, 6) + "19" + inumber.substr(6);
			date = ["19" + re[4], re[5], re[6]].join("-");
		}
		if(!this.IsDate(date, "ymd")) return false;
		var sum = 0;
		for(var i = 0;i<=16;i++){
			sum += Ai.charAt(i) * Wi[i];
		}
		Ai +=  verify.charAt(sum%11);
		return (inumber.length ==15 || inumber.length == 18 && inumber == Ai);
	},
	IsDate : function(op, formatString){
		formatString = formatString || "ymd";
		var m, year, month, day;
		switch(formatString){
			case "ymd" :
				m = op.match(new RegExp("^((\\d{4})|(\\d{2}))([-./])(\\d{1,2})\\4(\\d{1,2})$"));
				if(m == null ) return false;
				day = m[6];
				month = m[5]*1;
				year =  (m[2].length == 4) ? m[2] : GetFullYear(parseInt(m[3], 10));
				break;
			case "dmy" :
				m = op.match(new RegExp("^(\\d{1,2})([-./])(\\d{1,2})\\2((\\d{4})|(\\d{2}))$"));
				if(m == null ) return false;
				day = m[1];
				month = m[3]*1;
				year = (m[5].length == 4) ? m[5] : GetFullYear(parseInt(m[6], 10));
				break;
			default :
				break;
		}
		if(!parseInt(month)) return false;
		month = month==0 ?12:month;
		var date = new Date(year, month-1, day);
        return (typeof(date) == "object" && year == date.getFullYear() && month == (date.getMonth()+1) && day == date.getDate());
		function GetFullYear(y){return ((y<30 ? "20" : "19") + y)|0;}
	}
 }
