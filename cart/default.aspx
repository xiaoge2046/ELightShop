﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="cart_default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
<script language="javascript" type="text/javascript">
var iMath = {
    Mul : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) * Number(m); return (c / Math.pow(10, 4)) },
    Div : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) / Number(m); return (c.toFixed(2)) },
    Add : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) + Number(m); return (c / Math.pow(10, 2)) },
    Sub : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) - Number(m); return (c / Math.pow(10, 2)) }
};
var iKits={
	light_alert:function(tips){var obj01,obj02;if($3("#light_alert div.txt:0")){obj01=$3("#light_alert");obj02=$3("#light_fade")}else{var objN=document.createElement("div");var htmls="<div class=\"txt\">!</div>\n<div class=\"opera\"><span class=\"btn\" onclick=\"iKits.light_alert_close();\">CONFIRM</span></div>";objN.innerHTML=htmls;objN.setAttribute("id","light_alert");$3.box().appendChild(objN);$3("#light_alert div.txt:0").innerHTML=tips;var objM=document.createElement("div");objM.setAttribute("id","light_fade");$3.box().appendChild(objM);obj01=objN;obj02=objM;objN=objM=null}if(obj01!=undefined){obj01.style.display="block";obj01.style.left=parseInt(($3.box().clientWidth-obj01.clientWidth-18)/2)+"px";obj01.style.top=parseInt($3.elem().scrollTop+($3.elem().clientHeight/2)-(obj01.clientHeight/2)-18)+"px";obj02.style.display="block";obj02.style.width=$3.box().clientWidth+"px";obj02.style.height=$3.box().scrollHeight+"px";$3("#light_alert div.txt:0").innerHTML=tips;obj01=obj02=null}},
	light_alert_close:function(){$3("#light_alert").style.display="none";$3("#light_fade").style.display="none";$3("#light_fade").style.width=$3("#light_fade").style.height="5px"}
};

var redu8addi=function(way){
    var xtotal,gtotal;
    
    var elem_redu8addi=this.parent("tr").find("a.del8rec_link:0");
    var rsign=$3("#hid_rsign").value;
    var nmtotal = $3("#sub_total").innerHTML.replace(rsign, "");
    var ngtotal = $3("#gra_total").innerHTML.replace(rsign, "");
    var nprice = this.parent("tr").find("span.price:0").innerHTML.replace(rsign, "");
    var elem_result=this.parent("tr").find("span.result:0");
    var nresult = elem_result.innerHTML.replace(rsign, "");
    var elem_qty = this.parent("tr").find("input.inputs|quantity:0");
    //var elem_update = this.parent("tr").find("span.update:0");
    var nquantiy = isNaN(parseInt(elem_qty.value)) ? 1:parseInt(elem_qty.value);
    
    if(!gb_isdel[elem_redu8addi.name]){
        if(way==2){
            if($3.isie){
                document.execCommand("Undo");
            }else{
                elem_qty.value = parseInt(iMath.Div(nresult,nprice));
            }
        }
        return null;
    }
    
    if(way==0){
        if(nquantiy > 1){
            elem_qty.value = nquantiy = nquantiy-1;
        }
    }else if(way==1){
        elem_qty.value = nquantiy = nquantiy+1;
    }else if(way==2){
        if(nquantiy==0){ elem_qty.value = nquantiy = 1; }
    }
    
    $3("#hidctrl").src = "/aider/update.aspx?act=cart_cgnum&stand=" + elem_qty.value + "&uid=" + elem_redu8addi.name.replace("pro-","") + "&rnd=" + parseInt(Math.random() * 1000);
    
    nprice=iMath.Mul(nquantiy,nprice)
    xtotal=iMath.Sub(nmtotal, nresult);
    gtotal=iMath.Sub(ngtotal, nresult);
    
    elem_result.innerHTML = rsign + nprice;
    $3("#sub_total").innerHTML = rsign + iMath.Add(xtotal,nprice);
    $3("#gra_total").innerHTML = rsign + iMath.Add(gtotal,nprice);
   //elem_update.style.display="block";
    
    xtotal=gtotal=elem_redu8addi=rsign=nmtotal=ngtotal=nprice=elem_result=nresult=elem_qty=nquantiy=null;
}

var del8rec=function(){
    var xtotal,gtotal;
    var elem_del8rec=$3(this);
    var elem_result=$3(this).parent("tr").find("span.result:0");
    
    var rsign=$3("#hid_rsign").value;
    var nresult = elem_result.innerHTML.replace(rsign, "");
    var nmtotal = $3("#sub_total").innerHTML.replace(rsign, "");
    var ngtotal = $3("#gra_total").innerHTML.replace(rsign, "");
    
    if (elem_del8rec.innerHTML == "Remove") {
        xtotal = iMath.Sub(nmtotal, nresult);
        gtotal = iMath.Sub(ngtotal, nresult);
        if (xtotal == 0) {
            iKits.light_alert("This is the last!");
        } else {
            $3("#sub_total").innerHTML = rsign + xtotal;
            $3("#gra_total").innerHTML = rsign + gtotal;
            $3("#hidctrl").src = "/aider/update.aspx?act=cart_del!rec&stand=del&uid=" + elem_del8rec.name.replace("pro-","") + "&rnd=" + parseInt(Math.random() * 1000);
            elem_result.style.textDecoration = "line-through";
            elem_del8rec.innerHTML = "Recovery";
            gb_isdel[elem_del8rec.name]=false;
        }
    } else if (elem_del8rec.innerHTML == "Recovery") {
        xtotal = iMath.Add(nmtotal, nresult);
        gtotal = iMath.Add(ngtotal, nresult);
        $3("#sub_total").innerHTML = rsign + xtotal;
        $3("#gra_total").innerHTML = rsign + gtotal;
        $3("#hidctrl").src = "/aider/update.aspx?act=cart_del!rec&stand=rec&uid=" + elem_del8rec.name.replace("pro-","") + "&rnd=" + parseInt(Math.random() * 1000);
        elem_result.style.textDecoration = "none";
        elem_del8rec.innerHTML = "Remove";
        gb_isdel[elem_del8rec.name]=true;
    }
    elem_del8rec=elem_result=nresult=nmtotal=ngtotal=xtotal=gtotal=rsign=null;
};

var gb_isdel;
$3.onload(function(){
    if(!$3("#cart_prolist")){return null;}
    var stem="";
    $3("#cart_prolist a.del8rec_link").each(function(){
        $3(this).click(del8rec);
        stem=stem+",\""+$3(this).name+"\":true";
    });
    eval("gb_isdel={"+stem.substr(1)+"}");
    
    $3("#cart_prolist i.btn|redu").each(function(){
        $3(this).click(function(){ redu8addi.call($3(this),0) });
    });
    
    $3("#cart_prolist i.btn|addi").each(function(){
        $3(this).click(function(){ redu8addi.call($3(this),1) });
    });
    
    $3("#cart_prolist input.inputs|quantity").each(function(){
        if($3.isie){
			$3(this).attachEvent("onkeyup",function(){ redu8addi.call($3(this),2) });
		}else{
			$3(this).addEventListener("keyup",function(){ redu8addi.call($3(this),2) });
		}
    });
    stem=null;
});
</script>
</head>
<body>
<form id="xform" runat="server">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="CartDiv">
    <div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">CartList</a></div>
    <%if(isCartEmpty){%>
    <div class="cart_empty">
        <div class="box">
            <h3>Shopping Cart is Empty</h3>
            <div class="info">To put something in your cart, you can searching or browsing through categories navigation.<br/>When you find something you like, simply click "Add to Cart" and it will be placed here until you check out.<br/>Do you think this cart is broken? <a href="#">Click here for help</a></div>
            <div class="continue"><span class="inputs_btn_st01" onclick="history.go(-1);">Continue Shopping</span></div>
        </div>
    </div>
    <%}else{%>
    <div class="cart_prolist" id="cart_prolist">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_st01">
        <thead>
            <tr>
                <th width="13%" align="center" style="height: 16px">SKU</th>
				<th align="center" style="height: 16px">Item</th>
				<th width="12%" align="center" style="height: 16px">Unit Price</th>
				<th width="10%" align="center" style="height: 16px">Quantity</th>
				<th width="12%" align="center" style="height: 16px">Total Price</th>
				<th width="9%" align="center" style="height: 16px">Operation</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater id="Ft_List" runat="server">
            <ItemTemplate>
            <tr>
                <td align="center"><a href="/store/<%#Eval("static")%>.html">PAD16883<%#Eval("sku")%></a></td>
                <td>
                    <div class="pro_pic"><a href="/store/<%#Eval("static")%>.html"><img width="45" height="45" alt="" src="<%#Eval("small_pic")%>" /></a></div>
                    <div class="pro_txt">
                        <h2 class="ti"><a href="/store/<%#Eval("static")%>.html"><%#Eval("title")%></a></h2>
                        <p class="remark"><%#Eval("remark")%></p>
                    </div>
                </td>
                <td align="center"><span class="price"><%#Eval("xprice")%></span></td>
                <td align="center">
                    <i class="btn redu">reduce</i><input name="quantity" type="text" value="<%#Eval("amount")%>" size="3" maxlength="3" class="inputs quantity" autocomplete="off" /><i class="btn addi">addition</i>
                    <%--<span class="update" style="display: none;"><a href="javascript:void(0);" onclick="">update</a></span>--%>
                </td>
                <td align="center" class="eyec"><span class="result"><%#Eval("xresult")%></span></td>
                <td align="center"><a href="javascript:void(0);" class="del8rec_link" name="pro-<%#Eval("id")%>">Remove</a></td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="4" align="right">Subtotal:</td>
                <td align="left" colspan="2"><span class="total" id="sub_total"><asp:Literal id="Ft_ProTotal" runat="server"/></span><asp:Literal id="Ft_Hidden" runat="server"/><img id="hidctrl" class="hidctrl" alt="blank ctrl" src="/stylecss/images/blank.gif" /></td>
            </tr>
        </tbody>
        </table>
        <div class="mt10"></div>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_st01">
            <tr>
                <td>
                    <div class="fit_left">
                        <h3>Your Preferred Shipping Method (<a class="cblueulin" href="#">see details</a>)</h3>
                        <ul class="dlist">
                          <li><input type="radio" name="" checked="checked" class="radios" />Worldwide Shipping Only $10 (register airmail &amp; 6-10 business days)</li>
                          <%--<li><input type="radio" name="" class="radios" />Express Shipping</li>--%>
                        </ul>
                    </div>
                    <div class="fit_right">Shipping Cost:</div>
                    <p class="mclear"></p>
                </td>
                <td width="12%" align="center" class="eyec"><span class="price"><asp:Literal id="Ft_ShipCost" runat="server"/></span></td>
                <td width="10%"></td>
            </tr>
            <tr>
                <td>
                    <div class="fit_left">
                        <h3>Apply Coupon Code (<a class="cblueulin" href="#">see details</a>)</h3>
                        <ul class="dlist">
                            <li>Enter your coupon code if you have one.</li>
                            <li>
                                <input type="text" maxlength="20" size="29" class="inputs" name="coupon_code" />
                                <input type="button" class="inputs_btn_st01" value="Apply Coupon" name="coupon_submit" />
                            </li>
                            <li><span class="note">Note: </span>Special sale products could not be discounted with any coupons again.</li>
                        </ul>
                    </div>
                    <div class="fit_right">Coupon Discount:</div>
                    <p class="mclear"></p>
                </td>
                <td align="center" class="eyec"><span class="price">-US $0.00</span></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <div class="fit_left">
                        <span class="continue" onclick="history.go(-1);">&lt;&lt;Continue Shopping</span>
                    </div>
                    <div class="fit_right">Grand Total:</div>
                    <p class="mclear"></p>
                </td>
                <td colspan="2" align="left"><span class="total" id="gra_total"><asp:Literal id="Ft_GrandTotal" runat="server"/></span></td>
            </tr>
       </table>
        <div class="payment_method_l">
          <ul>
            <li><span class="se">1.</span><span class="txt">Please enter your correct shipping address in English while checking out with Paypal.We will ship your order to the address in your Paypal account;</span></li>
            <li><span class="se">2.</span><span class="txt">If you would like to ship your order to another address than the one in Paypal account,please make sure to edit your address in your Paypal account accordingly.</span></li>
          </ul>
        </div>
        <div class="payment_method_r">
            <h2>Payment Method</h2>
            <div class="tips">Please select the preferred payment method to use on this order.</div>
	        <div class="pay_btn"><a href="/cart/pay.html?shipping-method=registerairmail&payment-method=paypal" class="paypal"></a></div>
        </div>
        <p class="mclear"></p>
    </div>
    <%}%>
</div>
<!--CartDiv-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</form>
</body>
</html>
