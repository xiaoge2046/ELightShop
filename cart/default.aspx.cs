﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;

public partial class cart_default : System.Web.UI.Page
{
    public bool isCartEmpty = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        int nId = Convert.ToInt16(Request.QueryString["uid"]);
        int nQty = Convert.ToInt16(Request.QueryString["qty"]);
        string sStore = Request.QueryString["store"];
        string sColor = Request.QueryString["color"];
		string sAct = Request.QueryString["act"];

        Conn Tc = new Conn();
        string sCondi = string.Empty;
        string nNprice = "0";
		
		Ft_PageTitle.Text = "My Cart, OfferTablets.com";

        if (nId > 0 && nQty > 0)
        {
            if (sStore!="" && sStore!=null)
            {
                if (sStore.IndexOf("!") != -1)
                {
                    sStore = sStore.Substring(0, sStore.IndexOf("!"));
                }
                if(sStore.IndexOf("|") != -1)
                {
                    nNprice = sStore.Substring(sStore.IndexOf("|") + 1);
                    sStore = sStore.Substring(0, sStore.IndexOf("|"));
                }
                SqlDataReader drReaderM = Tc.ConnDate("select id from " + TableName.db_proxpvalue + " where proid=" + nId + " and pvalue like '%" + sStore + "%'");
                if (!drReaderM.HasRows)
                {
                    Other.net_alert("Execution Error!");
                }
                drReaderM.Close();
                drReaderM.Dispose();
                sStore = "store:" + sStore;
                sCondi = sCondi + sStore + "₪";
            }

            if (sColor != "" && sColor != null)
            {
                if (sColor.IndexOf("!") != -1)
                {
                    sColor = sColor.Substring(0, sColor.IndexOf("!"));
                }
                if (sColor.IndexOf("|") != -1)
                {
                    nNprice = sColor.Substring(sColor.IndexOf("|") + 1);
                    sColor = sColor.Substring(0, sColor.IndexOf("|"));
                }
                SqlDataReader drReaderN = Tc.ConnDate("select id from " + TableName.db_proxpvalue + " where proid=" + nId + " and pvalue like N'%" + sColor + "%'");
                if (!drReaderN.HasRows)
                {
                    Other.net_alert("Execution Error!");
                }
                drReaderN.Close();
                drReaderN.Dispose();
                sColor = "color:" + sColor;
                sCondi = sCondi + sColor + "₪";
            }

            if (Request.Cookies["MyCartIds"] == null || string.IsNullOrEmpty(Request.Cookies["MyCartIds"].ToString()))
            {
                Response.Cookies["MyCartIds"].Value = System.DateTime.Now.ToFileTime().ToString() + "-" + String.Format("{0:000}",System.DateTime.Now.Millisecond);
                SqlDataReader objUpData = Tc.ConnDate("insert into " + TableName.db_cart + "(proid,amount,nprice,ident,remark) values(" + nId + ",1," + nNprice + ",'" + Request.Cookies["MyCartIds"].Value + "',N'" + sCondi + "')");
                objUpData.Close();
                objUpData.Dispose();
            }
            else
            {
                string sCondiM = string.Empty;
                if (sCondi != string.Empty)
                {
                    sCondiM = "and remark=N'" + sCondi + "'";
                }
                SqlDataReader drReader = Tc.ConnDate("select id from " + TableName.db_cart + " where ident='" + Request.Cookies["MyCartIds"].Value + "' and proid=" + nId + sCondiM);
                if (drReader.Read())
                {
                    drReader.Close();
                    SqlDataReader objUpDataM = Tc.ConnDate("update " + TableName.db_cart + " set amount=amount+" + nQty + " where ident='" + Request.Cookies["MyCartIds"].Value + "' and proid=" + nId + sCondiM);
                    objUpDataM.Close();
                    drReader.Dispose();
                    objUpDataM.Dispose();
                }
                else
                {
                    drReader.Close();
                    SqlDataReader objUpDataN = Tc.ConnDate("insert into " + TableName.db_cart + "(proid,amount,nprice,ident,remark) values(" + nId + "," + nQty + "," + nNprice + ",'" + Request.Cookies["MyCartIds"].Value + "',N'" + sCondi + "')");
                    objUpDataN.Close();
                    drReader.Dispose();
                    objUpDataN.Dispose();
                }
            }
            if(sAct == "buynow")
			{
            	Response.Redirect("/cart/pay.html?shipping-method=registerairmail&payment-method=paypal&act=buynow");
			}
			else
			{
				Response.Redirect("/cart/");
			}
        }
        else
        {
            FCtrl xFctrl = new FCtrl();
            string[] aRate = xFctrl.Cvrate();
            double nPrice = 0;
            double nResult = 0;
            double nTotals = 0;
            double nShip = 0;
            DataTable Dtab = new DataTable();
            ArrayList Result = new ArrayList();
            try
            {
                SqlDataReader objUpData_DelMark = Tc.ConnDate("delete " + TableName.db_cart + " where delmark=1");
                objUpData_DelMark.Close();
                objUpData_DelMark.Dispose();

                Dtab = Tc.ConnDate("select atbm.id,atbm.proid,atbm.amount,atbm.nprice,atbm.remark,atbm.relea_time,atbn.title,atbn.price,atbn.small_pic,atbn.static,'-' xprice,'-' xresult,'-' sku from " + TableName.db_cart + " as atbm," + TableName.db_product + " as atbn where ident='" + Request.Cookies["MyCartIds"].Value + "' and atbm.proid=atbn.id order by atbm.relea_time desc", 0);

                if (Dtab.Rows.Count == 0)
                {
                    isCartEmpty = true;
                }
                else
                {
                    for (int i = 0; i < Dtab.Rows.Count; i++)
                    {
                        Dtab.Rows[i]["remark"] = Dtab.Rows[i]["remark"].ToString().Replace("₪", " ");

                        if (Dtab.Rows[i]["nprice"].ToString() == "0")
                        {
                            nPrice = Math.Round(Convert.ToDouble(Dtab.Rows[i]["price"]) / Convert.ToDouble(aRate[0]), 2);
                            Dtab.Rows[i]["xprice"] = aRate[1] + nPrice.ToString();
                            nResult = nPrice * Convert.ToInt16(Dtab.Rows[i]["amount"]);
                            Dtab.Rows[i]["xresult"] = aRate[1] + nResult.ToString();
                            Dtab.Rows[i]["sku"] = Dtab.Rows[i]["id"].ToString().PadLeft(6, '0');
                        }
                        else
                        {
                            nPrice = Math.Round((Convert.ToDouble(Dtab.Rows[i]["price"]) + Convert.ToDouble(Dtab.Rows[i]["nprice"])) / Convert.ToDouble(aRate[0]), 2);
                            Dtab.Rows[i]["xprice"] = aRate[1] + nPrice.ToString();
                            nResult = nPrice * Convert.ToInt16(Dtab.Rows[i]["amount"]);
                            Dtab.Rows[i]["xresult"] = aRate[1] + nResult.ToString();
                            Dtab.Rows[i]["sku"] = Dtab.Rows[i]["id"].ToString().PadLeft(6, '0');
                        }
                        nTotals = nTotals + nResult;
                    }
                    PagedDataSource objPds = new PagedDataSource();
                    objPds.DataSource = Dtab.DefaultView;

                    
                    Ft_List.DataSource = objPds;
                    Ft_List.DataBind();

                    Ft_ProTotal.Text = aRate[1] + nTotals.ToString();
                    Ft_Hidden.Text = "<input name=\"hid_rsign\" id=\"hid_rsign\" type=\"hidden\" value=\"" + aRate[1] + "\" />";

                    nShip = Math.Round(Si.shopcost / Convert.ToDouble(aRate[0]), 0);
                    Ft_ShipCost.Text = aRate[1] + nShip.ToString("F2");

                    Ft_GrandTotal.Text = aRate[1] + (nShip + nTotals);
                }
            }
            catch
            {
                isCartEmpty = true;
            }    
        }
        Tc.CloseConn();
    }
}
