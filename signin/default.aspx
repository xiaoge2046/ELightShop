﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="signin_default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
<script language="javascript" type="text/javascript">
var iKits={
	light_alert:function(tips){var obj01,obj02;if($3("#light_alert div.txt:0")){obj01=$3("#light_alert");obj02=$3("#light_fade")}else{var objN=document.createElement("div");var htmls="<div class=\"txt\">!</div>\n<div class=\"opera\"><span class=\"btn\" onclick=\"iKits.light_alert_close();\">CONFIRM</span></div>";objN.innerHTML=htmls;objN.setAttribute("id","light_alert");$3.box().appendChild(objN);$3("#light_alert div.txt:0").innerHTML=tips;var objM=document.createElement("div");objM.setAttribute("id","light_fade");$3.box().appendChild(objM);obj01=objN;obj02=objM;objN=objM=null}if(obj01!=undefined){obj01.style.display="block";obj01.style.left=parseInt(($3.box().clientWidth-obj01.clientWidth-18)/2)+"px";obj01.style.top=parseInt($3.elem().scrollTop+($3.elem().clientHeight/2)-(obj01.clientHeight/2)-18)+"px";obj02.style.display="block";obj02.style.width=$3.box().clientWidth+"px";obj02.style.height=$3.box().scrollHeight+"px";$3("#light_alert div.txt:0").innerHTML=tips;obj01=obj02=null}},
	light_alert_close:function(){$3("#light_alert").style.display="none";$3("#light_fade").style.display="none";$3("#light_fade").style.width=$3("#light_fade").style.height="5px"}
};
function checkf(){
    var email_patt=/\w+@.+\..+$/;
    if($3("#emailaddress").value==""){
        iKits.light_alert("Please enter your Email Address.");
        return false;
    }
    if(email_patt.test($3("#emailaddress").value)==false){
        iKits.light_alert("Please enter a valid Email Address.");
        return false;
    }
    if($3("#password").value==""){
        iKits.light_alert("Please enter your Password.");
        return false;
    }
    document.xform.submit();
    return true;
}
</script>
</head>
<body>
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="SimPage">
    <div class="xlocation"><span class="ico"></span><a href="/">Home</a> &gt; Sign In</div>
    <div class="sign_block">
		<div class="box">
			<form action="/signin/" method="post" id="xform" name="xform" runat="server">
        	<h3>Sign In To View Your Account</h3>
        	<p class="signin_tips">Please sign in to your OfferTablets.com account. If you do not have an account, <a href="/register/">Register now</a>.</p>
            <ul class="inputs_list">
                <li><span class="tips"><em>*</em>Email Address:</span><input name="emailaddress" type="text" class="inputs" id="emailaddress" size="29" maxlength="80" runat="server" /></li>
                <li><span class="tips"><em>*</em>Password:</span><input name="password" type="password" class="inputs" id="password" size="29" maxlength="20" runat="server" /></li>
                <li class="gpassw_tips"><span class="tips">&nbsp;</span><a href="#">Forgot your password?</a></li>
            	<li><span class="tips">&nbsp;</span><input type="button" name="signin_btn" id="signin_btn" value="Sign In" class="inputs_btn_st01" style="width: 80px;" onclick="checkf();" /></li>
            </ul>
			</form>
      </div>
	</div>
</div><!--SimPage-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
<asp:Literal id="Ft_Lalert" runat="server"/>
</body>
</html>