﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;

public partial class signin_default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		Ft_PageTitle.Text = "Sign In to Your Account, OfferTablets.com";
        #region
        if (IsPostBack)
        {
            string md5_pass = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(password.Value, "MD5").ToLower().Substring(8, 16);
            Conn Tc = new Conn();
            SqlDataReader objDataReader = Tc.ConnDate("select username,id from " + TableName.db_member + " where username='" + emailaddress.Value + "' and passwd='" + md5_pass + "'");
            if (objDataReader.Read())
            {
                Session["Member"] = objDataReader["username"].ToString();
                Session["MemberID"] = objDataReader["id"].ToString();
                objDataReader.Close();
                SqlDataReader objUpData = Tc.ConnDate("update " + TableName.db_member + " set login_time='" + DateTime.Now + "'");
                Response.Redirect(Si.siteurl + "account/?valid=real");
                objUpData.Dispose();
            }
            else
            {
                Response.Redirect(Si.siteurl + "signin/?status=E8981");
            }
            objDataReader.Dispose();
            Tc.CloseConn();
        }
        #endregion
        string sLalert = "<div><script language=\"javascript\" type=\"text/javascript\">iKits.light_alert(\"{$tips}\");</script></div>";
        string errors = Request.QueryString["status"];
        switch (errors)
        {
            case "E8981":
                Ft_Lalert.Text = sLalert.Replace("{$tips}", "Invalid Email Address/Password.");
                break;
            default:
                break;
        }
    }
}
